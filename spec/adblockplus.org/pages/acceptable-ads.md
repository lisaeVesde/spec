# Acceptable Ads page

## Page information

| Key | Value |
| :-- | :-- |
| Locales | `EN`, `DE`, `ES`, `FR`, `NL`, `PT-BR`  |
| URL | `acceptable-ads` |

**Note:** The languages we support, update and maintain are `EN`, `DE`, `ES`, `FR`, `PT-BR`. The page is only available in `NL` thanks to our contributors on Crowdin but it is not a language we actively translate our website into.

## Functionality

The page uses the [default template](/spec/adblockplus.org/templates/default.md) for displaying content on ABP.org.

The page also contains the [table of contents](/spec/adblockplus.org/templates/default.md#table-of-contents) for easy navigation.

## Meta data

| Key | Value |
| :-- | :-- |
| title | Allowing acceptable ads in Adblock Plus |
| description | Adblock Plus introduces the Acceptable Ads initiative: Support websites that rely on advertising but choose to do it in a non-intrusive way. |
| noheading | true |

## Content

The heading of the page is automatically taken by the CMS from the title of the page.

### Alert message

An alert is displayed at the beginning of the page, beneath the table of contents.

``` 
This page is under review as management of the Acceptable Ads initiative has been transferred to the independent [Acceptable Ads Committee](https://acceptableads.org/).
```

### "What is it about?" section

``` 
## What is this about?

Adblock Plus is a tool that lets users block [ads](https://easylist.to/2011/07/11/the-definition-of-advert-and-link-exchange-policy.html). Since ads fuel a lot of the content we enjoy for free online, finding common ground seemed to make sense. We asked our users about this and they overwhelmingly agreed.

So starting in 2011, in consultation with [our users](https://adblockplus.org/blog/adblock-plus-user-survey-results-part-0), we decided to propose a compromise. Because we share a vision with the majority of our users that not all ads are equally annoying, the Acceptable Ads initiative was created. It allows advertisers and publishers who have agreed to make ads that abide by user-generated criteria to be whitelisted. Users can support this less extreme version of ad blocking by allowing the Acceptable Ads option to remain enabled. To browse completely ad-free, users can [disable the option](#optout).

The Acceptable Ads initiative is beneficial because it encourages the ad industry to pursue less intrusive ad formats, thus having a positive impact on the Internet as a whole. It also provides us with a [viable source of revenue](about#monetization), paid only by larger participants in the Acceptable Ads initiative, which we need in order to be able to administer and maintain the program and continue development of a free product.

We believe that this initiative is a sustainable middle ground between the user's choice to use ad blockers and the continued need to support free online content with advertisements. In general, an Acceptable Ad is a non-animated ad, clearly labeled as such, that does not interrupt reading flow. It is important to note that these criteria try to reflect the ongoing discussions between our users, publishers, advertisers and us. As [previously announced](https://adblockplus.org/blog/campdavid-nyc), in 2017 eyeo / Adblock Plus transferred management of the Acceptable Ads initiative to an independent group of people, the [Acceptable Ads Committee](https://acceptableads.org/).

To ensure transparency and fairness, the following rules have and will always apply to everyone without exceptions:

- Participants cannot pay to avoid the criteria. **Every ad has to comply with the criteria**.
- For transparency reasons, we add all Acceptable Ads to our forum to provide our community with the opportunity to submit feedback. We greatly value feedback and read all comments.
- Adblock Plus users are valuable to us and we listen to them. If, for valid reasons, any Acceptable Ads proposal is rejected by our community, the ad(s) will be removed from our whitelist.

We are able to keep our open source product free [by charging large entities a fee for whitelisting](about#monetization) services. For the other roughly 90 percent of our partners, these services are offered free of charge.

Do you think this is a good idea? Show your support by signing our [Acceptable Ads Manifesto](https://acceptableads.org/).
```

### "What is an Acceptable Ad?" section

Asset(s):

- [Ad placement](/res/adblockplus.org/screenshots/acceptable-ads-criteria-placement.png)
- [Ad distinction](/res/adblockplus.org/screenshots/acceptable-ads-criteria-label.png)
- [Ad percentage](/res/adblockplus.org/screenshots/acceptable-ads-criteria-percentages.png)

``` 
## What is an Acceptable Ad?

Ads that shall be treated as Acceptable Ads have to comply with the following criteria:
```

#### General criteria

```
### General criteria

#### Placement

Ads must not disrupt the user's natural reading flow. Such ads must be placed on top, side or below the Primary Content <sup>[1](#primary-content)</sup>.

[Acceptable ads criteria placement](acceptable-ads-criteria-placement.png)

#### Distinction

Ads should always be recognizable as ads, and distinguishable from all other content (e.g. are not hiding the label, are not misleading users into thinking an ad is part of the primary content). Ads should be clearly marked with the word "advertisement" or its equivalent.

[Acceptable ads criteria label](acceptable-ads-criteria-label.png)

#### Size

Individual ad-size requirements depend on the placement of the ad:

- When placed above the primary content, the maximum height of an ad should be 200px.
- When placed on the side of the primary content, the maximum width of an ad should be 350px.
- When placed below the primary content, the maximum height of an ad should be 400px.

Ads must always leave sufficient space for the Primary Content on the common screen size of 1366x768<sup>[2](#screen-desktop)</sup> for desktop, 360x640<sup>[3](#screen-mobile)</sup> for mobile devices and 768x1024<sup>[4](#screen-tablets)</sup> for tablets.

All ads that are placed above the fold (the portion of the web page visible in the browser window when the page first loads under the common screen size), must not occupy in total more than 15 percent of the visible portion of the web page. If placed below the fold, ads must not occupy in total more than 25 percent of the visible portion of the webpage.

[Acceptable ads criteria percentage](acceptable-ads-criteria-percentages.png)
```

#### Specific criteria

```
### Specific criteria

#### Text ads
Text ads designed with excessive use of colors and/or other elements to grab attention are not permitted.

#### Image ads
Static image ads may qualify as acceptable, according to an evaluation of their unobtrusiveness based on their integration on the webpage.

#### In-feed ads
For ads in lists and feeds, the general criteria differ depending on:

- Placement requirements 
  Ads are permitted in between entries and feeds.
- Size requirements 
  In-feed ads are permitted to take up more space, as long as they are not substantially larger than other elements in the list or feed.

#### Search ads

For search ads - ads displayed following a user-initiated search query - the criteria differ depending on:

- Size requirements 
  Search ads are permitted to be larger and take up additional screen space.

#### Ads on pages with no primary content

Only text ads are allowed. For webpages without any primary content (e.g. error or parking pages), the criteria differ depending on:

- Placement requirements 
  No placement limitations.
- Size requirements 
  No size limitations.

#### Mobile Ads

##### Placement Requirements

- Static ad types (e.g. 6x1 banner and 1x1 tile ad) are allowed to be placed anywhere on the mobile page
- Small ads (6x1 banner or smaller) are allowed to be placed as a sticky ad on the bottom of the screen. Other formats are not allowed to stick.
- Large ad types (e.g. native tile ads) are only allowed to be placed under the Primary Content.<sup>[1](#fn:1)</sup> <sup>[2](#fn:2)</sup>

##### Size Requirements

Ads showing on mobile screens are bound to the following size restrictions:

- Ads implemented on the scrollable portion of the webpage must not occupy in total more than 50 percent of the visible portion of the webpage.<sup>[3](#fn:3)</sup>
- Ads implemented as a ‘sticky ad’ have a maximum height restriction of 75px (or 15%).
- Below the Primary Content, ads are limited to 100% of the screen space.<sup>[4](#fn:4)</sup>

##### Animations

Animations are allowed for the 6x1 ad type when placed as a ‘sticky’ ad on the bottom of the screen. Animations have to comply with the LEAN standard for animations, and a close button or some other closing mechanism must be included.<sup>[5](#fn:5)</sup>

#### Other Acceptable Ads formats?

Are your ads displayed on alternative screens, or are you convinced that you have an innovative Acceptable Ads format which doesn't fit the ads outlined above? [Let us know!](acceptableads@adblockplus.org)

<sup>1</sup> The 'Primary Content' is defined as (based on [Mozilla's description>(https://developer.mozilla.org/en-US/docs/Web/HTML/Element/main) of the <main> HTML element): The Primary Content consists of content that is directly related to, or expands upon the central topic of a document or the central functionality of an application. This content should be unique to the document, excluding any content that is repeated across a set of documents such as sidebars, navigation links, copyright information, site logos, and search forms (unless, of course, the document's main function is a search form).

<sup>2</sup> The 'common screen size' for desktop is 1366x768, based on data from [StatCounter](http://gs.statcounter.com/#desktop-resolution-ww-monthly-201401-201412).

<sup>3</sup> The 'common screen size' for mobile is 360x640, based on data from [StatCounter](http://gs.statcounter.com/#mobile_resolution-ww-monthly-201401-201412).

<sup>4</sup> The 'common screen size' for tablets is 768x1024, based on data from [StatCounter](http://gs.statcounter.com/#tablet-resolution-ww-monthly-201401-201412).
```

### "What are Acceptable Ads without third-party tracking?" section

``` 
## What are Acceptable Ads without third-party tracking?

Acceptable Ads without third-party tracking are ads that comply with the [Acceptable Ads criteria](https://acceptableads.com/en/about/criteria) and that do not allow third-party entities to track any of your browsing behavior. These are ads that comply with [Do Not Track](https://www.eff.org/dnt-policy), and/or ads which are served by the domain which is wholly owned by the same company.
```

### "What is not considered an Acceptable Ad?" section

``` 
## What is not considered an Acceptable Ad?

The following types of ads are currently unacceptable<sup>[*](#exception)</sup>, and cannot be considered for inclusion on the whitelist:

- Ads that visibly load new ads if the Primary Content does not change
- Ads with excessive or non user-initiated hover effects
- Animated ads<sup>[6](#fn:6)</sup>
- Autoplay-sound or video ads
- Expanding ads
- Generally oversized image ads
- Interstitial page ads
- Overlay ads
- Overlay in-video ads
- Pop-ups
- Pop-unders
- Pre-roll video ads
- Rich media ads (e.g. Flash ads, Shockwave ads, etc.)

<sup>*</sup>Except when the user intentionally interacts with the ad (e.g. clicks on the ad to see a video ad playing).
```

### "Why is this feature enabled by default?" section

``` 
## Why is this feature enabled by default?

Unfortunately, this is the only way to accomplish the goals described above. If the majority of Adblock Plus users have this function activated, advertisers will have the incentive to produce better ads.
```

### "But I hate all ads!" section

``` 
## But I hate all ads!

No problem, you can disable this feature at any time:

**Chrome (ABP versions 3.0.3+), Firefox (ABP versions 3+), Opera (ABP versions 3.0.3+)** - Click the Adblock Plus icon and select **Options**. Uncheck **Allow Acceptable Ads**.

**Chrome (ABP versions <3.0.3), Maxthon, Opera (ABP versions <3.0.3), Safari** - Click the Adblock Plus icon and select **Options**. Uncheck **Allow some nonintrusive advertising**.

**Firefox (ABP versions 1 and 2)** - Click the Adblock Plus icon and select **Filter preferences**. Uncheck **Allow some nonintrusive advertising**.

**Internet Explorer** - Click the Adblock Plus icon and select **Settings**. Uncheck **Allow some nonintrusive advertising**.

**Adblock Browser for Android** - Open Settings, tap **Ad blocking > Configure Acceptable Ads** and uncheck **Allow some nonintrusive advertising**.

**Adblock Browser for iOS** - Open Settings, tap **Adblock Plus > Acceptable Ads** and uncheck **Allow some nonintrusive ads**.

**Adblock Plus for iOS** - Open Adblock Plus for iOS from the Home screen, tap the **Settings** icon > **Acceptable Ads** and uncheck **Allow some nonintrusive ads**.
```

### "Will all "acceptable" ads be unblocked?" section

``` 
No. Unfortunately, it isn't technically possible to recognize ads that meet our Acceptable Ads criteria automatically. We have [agreements](https://adblockplus.org/acceptable-ads-agreements) with some websites and advertisers which stipulate that only advertising matching our criteria will be displayed when Adblock Plus users visit these particular sites. These ads will be unblocked (i.e. added to the Acceptable Ads exception list which is enabled per default). No applicant will be favored or treated differently, and no one can buy their way onto the whitelist. **Everyone must comply** with the criteria and everyone goes through the same process before the ads qualify as "acceptable".
```

### "What if an ad is allowed, but doesn't meet the requirements?" section

``` 
## What if an ad is allowed, but doesn't meet the requirements?

Please [report it](#false-negatives). If an advertiser abuses its placement on the exception list, we can always remove it from the list.
```

### "How can I see what you are allowing?" section

``` 
## How can I see what you are allowing?

The **Allow nonintrusive advertising** checkbox simply adds one more filter subscription to your list. You can view the filters [here](https://easylist-downloads.adblockplus.org/exceptionrules.txt). Also, the special treatment of this filter subscription - which was added for reasons of usability - can be disabled by going to "about:config" and changing the [extensions.adblockplus.subscriptions_exceptionscheckbox](#subscriptions_exceptionscheckbox) preference to false. This will allow you to view the filters for this subscription as usual.

Do you have questions or suggestions concerning this list? Feel free to get in touch with the community in our [forum](https://adblockplus.org/forum) or contact us directly [via email](acceptableads@adblockplus.org).
```

### "How can I get my website whitelisted?" section

``` 
## How can I get my website whitelisted?

The process of getting your ads whitelisted takes about 10 working days once the ads adhere to the Acceptable Ads criteria:

1. If your website has ads that comply with the [Acceptable Ads criteria](#criteria), simply fill out [this form](https://acceptableads.com/get-whitelisted/).
2. Someone from [Eyeo](https://www.eyeo.com/), the company behind Adblock Plus, will contact you to determine the exact ads, and check whether they comply with our criteria.
3. After you have made any necessary changes, both sides sign an [agreement](https://adblockplus.org/acceptable-ads-agreements).
4. We submit the whitelisting proposal [in the forum](https://adblockplus.org/forum/viewforum.php?f=12) and the ads are whitelisted at the same time. The topic will stay open in order for the community to declare concerns if or when the candidate does not meet the requirements.
```

### "Do Adblock Plus users really want this feature?" section

``` 
## Do Adblock Plus users really want this feature?

The [results of our user survey](https://adblockplus.org/blog/adblock-plus-user-survey-results-part-3) say yes. According to the survey, only 25 percent of Adblock Plus users are strictly against all advertising. They can disable the feature and browse completely ad-free. The other 75 percent replied that they would accept some advertising to help support websites.
```

### "Is there payment involved?" section

``` 
## Is there payment involved?

We receive some donations from our users, but our main source of revenue comes as part of the Acceptable Ads initiative. Larger entities (as defined below) pay a licensing fee for the whitelisting services requested and provided to them (around 90 percent of the licences are granted for free). It should be noted that the Acceptable Ads criteria must be met independent of the consideration for payments. If the criteria are not met, whitelisting is impossible.

Regarding fees, only large entities (those with more than 10 million additional ad impressions per month due to participation in the Acceptable Ads initiative) have to pay. For these entities, our licensing fee normally represents 30 percent of the additional revenue created by whitelisting its acceptable ads.

For more details see [here](about#monetization).
```

### Footnotes section

``` 
1. Large ad: any ad >300px height [↩](#fnref:1)

2. The 'Primary Content' is defined as (based on [Mozilla's description](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/main) of the <main> HTML element): The Primary Content consists of content that is directly related to, or expands upon the central topic of a document or the central functionality of an application. This content should be unique to the document, excluding any content that is repeated across a set of documents such as sidebars, navigation links, copyright information, site logos, and search forms (unless, of course, the document's main function is a search form). [↩](#fnref:2)

3. The visible portion of the webpage is defined as a standard CSS pixel size of [360x512px](https://drive.google.com/file/d/1JGn_-qdlAqMthA6OFImCdpMH7j3OxIFQ/view?usp=sharing) (Samsung Galaxy S7 with the SBrowser), which is based on the standard viewport of [360x640px](https://deviceatlas.com/blog/mobile-viewport-size-statistics-2017), but with the OS- and browser UI elements deducted. [↩](#fnref:3)

4. This means that users can scroll past the primary content and an ad unit served after it can take up the whole screen; but following this ad unit, additional ads cannot be implemented. [↩](#fnref:4)

5. From: [Iab New Standard Ad Unit Portfolio (2017)](https://www.iab.com/wp-content/uploads/2017/08/IABNewAdPortfolio_FINAL_2017.pdf) [↩](#fnref:5)

6. With one exception, please see [Mobile Ads](#mobile-ads). [↩](#fnref:6)
```